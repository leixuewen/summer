module.exports = {
    // 打包时不生成.map文件 避免看到源码
    productionSourceMap: false,
    pluginOptions: {
        electronBuilder: {
            builderOptions: {
                appId: 'io.github.leixuewen',
                productName: 'summer',
                copyright: 'Copyright © 2019 summer',
                artifactName: '${productName}-Setup-${version}.${ext}',
                // 设置发布清单(用于生成latest.yml文件,github公开仓库默认以当前项目信息自动填充)
                // publish: {
                //     provider: 'generic',
                //     url: ''
                // }
            }
        }
    }
};