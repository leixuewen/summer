'use strict';

import {autoUpdater} from "electron-updater"
import {app, protocol, Notification, BrowserWindow} from 'electron'
import {
    createProtocol,
    installVueDevtools
} from 'vue-cli-plugin-electron-builder/lib'

const isDevelopment = process.env.NODE_ENV !== 'production';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([{scheme: 'app', privileges: {secure: true, standard: true}}])

function createWindow() {
    // Create the browser window.
    win = new BrowserWindow({
        width: 800, height: 600, webPreferences: {
            nodeIntegration: true
        }
    });

    if (process.env.WEBPACK_DEV_SERVER_URL) {
        // Load the url of the dev server if in development mode
        win.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
        if (!process.env.IS_TEST) win.webContents.openDevTools()
    } else {
        createProtocol('app');
        // Load the index.html when not in development
        win.loadURL('app://./index.html')
    }

    win.on('closed', () => {
        win = null
    })
}

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
    if (isDevelopment && !process.env.IS_TEST) {
        // Install Vue Devtools
        try {
            await installVueDevtools()
        } catch (e) {
            console.error('Vue Devtools failed to install:', e.toString())
        }
    }
    createWindow();
    autoUpdate();
});

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
    if (process.platform === 'win32') {
        process.on('message', data => {
            if (data === 'graceful-exit') {
                app.quit()
            }
        })
    } else {
        process.on('SIGTERM', () => {
            app.quit()
        })
    }
}

function autoUpdate() {
    if (process.env.WEBPACK_DEV_SERVER_URL) return;

    autoUpdater.setFeedURL('http://localhost:8080/dist_electron');

    autoUpdater.on('error', function (error) {
        new Notification({
            body: '自动更新异常' + error
        }).show();
    });

    autoUpdater.on('checking-for-update', function () {
        new Notification({
            body: '检查更新'
        }).show();
    });

    autoUpdater.on('update-available', function () {
        new Notification({
            body: '更新可用'
        }).show();
    });

    autoUpdater.on('update-not-available', function () {
        new Notification({
            body: '更新不可用'
        }).show();
    });

    // 更新下载进度事件
    autoUpdater.on('download-progress', () => {
        new Notification({
            body: '更新下载进度事件'
        }).show();
    });

    // 下载完成事件
    autoUpdater.on('update-downloaded', () => {
        new Notification({
            body: '下载完成事件'
        }).show();
        autoUpdater.quitAndInstall();
    });


    //执行自动更新检查
    autoUpdater.checkForUpdates();
}